package nl.hu.fnt.gsos.rest;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import nl.hu.fnt.gsos.service.Track;
import nl.hu.fnt.gsos.service.TrackServiceImpl;

@Path("/tracks")
public class TrackService {

    TrackServiceImpl tsi = new TrackServiceImpl();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTracks() {

        List<Track> tracks = tsi.getTracks();
        return Response.status(200).entity(tracks).build();

    }

    @GET
    @Path("/count")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTracksCount() {

        int i = tsi.getTracks().size();
        String output = "amount of tracks: " + i;

        return Response.status(200).entity(output).build();

    }

    @GET
    @Path("/1")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTrack() {
        
        Track t = tsi.getTrackById(1);
        String output = "" + t.toString();

        return Response.status(200).entity(t).build();

    }

    @PUT
    @Path("/add")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addTrack() {
        
        Track i = tsi.getTrackById(2);
        
        tsi.remove(i.getId());
        
        tsi.addTrack(i);
        
        
        String output = i.toString() +" added";

        return Response.status(200).entity(output).build();

    }

    @DELETE
    @Path("/delete")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delTrack() {
        Track i = tsi.getTrackById(3);
        tsi.remove(i.getId());
        
        String output = i.toString() + " removed";

        return Response.status(200).entity(i).build();

    }

}
